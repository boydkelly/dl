#%global commit 95ddd43cdde907e7439cd7e85bf32837112770d7
#%global shortcommit %(c=%{commit}; echo ${c:0:7}) 
%global gittag 3.6.0 

Name:     vimb
Version:  3.6.0
Release:  1%{dist} 
Summary:  The vim-like browser
License:  GPL-3.0-or-later
URL:      https://fanglingsu.github.io/vimb

Source0:  https://github.com/fanglingsu/vimb/archive/%{gittag}/%{name}-%{version}.tar.gz

BuildRequires:  gtk3-devel
BuildRequires:  webkit2gtk3-devel 

%description
vimb is a WebKit-based web browser that behaves like the vimperator
plugin for Firefox, and has usage paradigms from the editor vim.

%prep
%setup -q

%build
make %{?_smp_mflags} PREFIX=%{_prefix}

%install
%make_install PREFIX=%{_prefix}

%files
%{_bindir}/vimb
%dir %{_prefix}/lib/vimb
%{_prefix}/lib/vimb/webext_main.so
%{_datadir}/applications/vimb.desktop
%{_mandir}/man1/vimb.1%{?ext_man}

%changelog

