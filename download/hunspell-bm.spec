Name: hunspell-bm
Summary: Bambara hunspell dictionaries
%global upstreamid 024
Version: 0.%{upstreamid}
Release: 17%{?dist}
Source: https://extensions.openoffice.org/de/download/18920
URL: http://extensions.services.openoffice.org/project/dict-bm
License: BSD-3
BuildArch: noarch

Requires: hunspell
Supplements: (hunspell and langpacks-bm)

%description
Bambara hunspell dictionaries.

%prep
%autosetup -c -n hunspell-bm

%build
for i in desc_*.txt LICENCES*.txt; do
  if ! iconv -f utf-8 -t utf-8 -o /dev/null $i > /dev/null 2>&1; then
    iconv -f ISO-8859-1 -t UTF-8 $i > $i.new
    touch -r $i $i.new
    mv -f $i.new $i
  fi
  tr -d '\r' < $i > $i.new
  touch -r $i $i.new
  mv -f $i.new $i
done

%install
mkdir -p $RPM_BUILD_ROOT/%{_datadir}/myspell
cp -p bm.dic $RPM_BUILD_ROOT/%{_datadir}/myspell/bm.dic
cp -p bm.aff $RPM_BUILD_ROOT/%{_datadir}/myspell/bm.aff

%files
%doc desc_*.txt 
%license LICENCES*.txt
%{_datadir}/myspell/*

%changelog
* Wed Apr 14 2021 Boyd Kelly <bkelly@fedoraproject.org> - 024
- hunspell for Bambara - initial build 024

