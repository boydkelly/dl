%global commit 4e3dc13e75f10993bd9a691e6f34c8316fb36ce8
%global shortcommit %(c=%{commit}; echo ${c:0:7})
%global gittag hledger-1.20.3
%global debug_package %{nil}

Name:     hledger
Version:  1.20.3
Release:  1%{dist}
Summary:  hledger
License:  GPL-3.0
URL:      https://hledger.org
 
Source0:  https://github.com/simonmichael/hledger/archive/%{commit}/%{name}-%{commit}.tar.gz

BuildRequires: stack
BuildRequires: ghc
BuildRequires: glibc-langpack-en
BuildRequires: perl
BuildRequires: gcc
BuildRequires: make
BuildRequires: automake
BuildRequires: gmp-devel
BuildRequires: libffi
BuildRequires: zlib-devel
BuildRequires: tar
BuildRequires: git
BuildRequires: gnupg

%description
hledger is a computer program for easily tracking money, time, or other commodities, on unix, mac and windows (and web-capable mobile devices, to some extent).

%prep
%autosetup -n %{name}-%{commit}

%build
#( curl -sSL https://get.haskellstack.org/ | sh )
LANG=en_US.UTF-8 stack setup
LANG=en_US.UTF-8 stack build

%install
stack install
mkdir -p -m 755  %{buildroot}%{_bindir}/
install -m 755  ~/.local/bin/* %{buildroot}%{_bindir}/

%post

%files
/%_bindir/*

%changelog
* Tue Jan 19 2021 Boyd Kelly <bkelly@coastsystems.net> - 1.20.3
- Update to 1.20.1
* Sun Dec 13 2020 Boyd Kelly <bkelly@coastsystems.net> - 1.20.1
- Update to 1.20.1
* Fri Dec 11 2020 Boyd Kelly <bkelly@coastsystems.net> - 1.20.0
- Update to 1.20.0
* Wed Sep 9 2020 Boyd Kelly <bkelly@coastsystems.net> - 1.19.1
- Update to 1.19.1
* Mon Sep 7 2020 Boyd Kelly <bkelly@coastsystems.net> - 1.19.0
- Initial build of hledger 1.19.0  

